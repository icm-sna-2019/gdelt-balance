#!/bin/sh
# A simple script that downloads a sample month

FINAL_FILE_NAME=mentions.csv
DATE_STR=201606

mkdir -p ./dataset

curl "http://data.gdeltproject.org/blog/2018-news-outlets-by-country-may2018-update/MASTER-GDELTDOMAINSBYCOUNTRY-MAY2018.TXT" > ./dataset/sourcesbycountry.csv

curl "http://data.gdeltproject.org/gdeltv2/masterfilelist.txt" > ./dataset/masterfilelist.txt
for url in $(grep -E "^.+$DATE_STR[0-9]+\.mentions.CSV.zip" dataset/masterfilelist.txt | awk '{print $3}')
do
    echo $url
    csv_name=$(echo $url | grep -E -o "[0-9]+.mentions.CSV")
    curl $url > ./dataset/$csv_name.zip
    unzip ./dataset/$csv_name.zip -d ./dataset
    cat ./dataset/$csv_name >> ./dataset/$FINAL_FILE_NAME
    rm ./dataset/$csv_name.zip ./dataset/$csv_name
done

rm ./dataset/masterfilelist.txt
