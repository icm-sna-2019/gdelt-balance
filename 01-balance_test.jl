## Helper types
include("src/sorted_pair.jl")

## Data parsing constants and definitions
include("src/dataset.jl")

CHUNKSIZE = 200_000
TOTAL_CHUNKS = 10
OVERLAP = 0.0 # overlap between chunks

# Structural balance analysis algorithms
## GDELT data to network conversion
include("src/agreement.jl")

## Triad detection and census computation
include("src/triadic_census.jl")

## Structural balance tests
include("src/tests.jl")

# Demo
println("Loading data...")
first_chunk = first(gdelt_iterator(CHUNKSIZE, TOTAL_CHUNKS, overlap=OVERLAP))
graph, source_map, sign_map = compute_agreement_graph(first_chunk)
triad_collection = triads(graph)

n_tests = 10
println("Testing how number of triads of type one in the measured graph deviate from the null model over $(n_tests) simulations...")
z_score = test(graph, sign_map, triad_collection, 1, n_tests)
println("The z-score is $(z_score)")

