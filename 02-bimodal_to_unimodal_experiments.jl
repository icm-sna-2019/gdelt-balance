"""
# 02-bimodal_to_unimodal_experiments.jl

This script examines whether our methodology of transforming the signed bimodal
outlet-event sentiment network into a signed unimodal outlet agreement network
results in structural balance by chance.

We do so by executing a number of permutation tests, and comparing their census
statistics to our network. If random networks are similar to ours, then there
is a high chance our methodology is flawed.
"""

using ProgressMeter
using Plots

include("src/sorted_pair.jl")
include("src/agreement.jl")
include("src/triadic_census.jl")
include("src/dataset.jl")
include("src/tests.jl") # shuffle_signs
include("src/sentiment_dict.jl")

s_src = 1:40
s_events = 1:40

measure_neg_fraction_in_data(data) = nrow(data[data.MentionDocTone .< 0, :])/nrow(data)

"""
    shuffle_test(sentiment_dict; N=10)

Shuffles the `sentiment_dict` `N` times and computes the resulting censuses.
Returns the mean of each triad type.
"""
function shuffle_test(sentiment_dict; N=10)
    counts = [0, 0, 0, 0]
    for i in 1:N
        sentiment_dict = shuffle_signs(sentiment_dict)
        graph, source_map, sign_map = sentiment_dict_to_agreement(sentiment_dict) |> dict_to_graph
        census = triadic_census(graph, sign_map)
        for (type, count) in census
            counts[type + 1] += census[type]
        end
    end
    triad_count = combinations(s_src, 3) |> length
    return counts ./ N ./ triad_count
end

"""
    test_neg_fraction(neg_fraction; N=10)

Runs `N` permutation tests over a given `(source, event) => sentiment`
dictionary `dict. That is, the topology of the dictionary is maintained while
the values are shuffled.
"""
function test_neg_fraction(neg_fraction; N=10)
    counts = [0, 0, 0, 0]
    for i in 1:N
        sentiments = sentiment_dict_random(s_src, s_events, neg_fraction)
        graph, source_map, sign_map = sentiment_dict_to_agreement(sentiments) |> dict_to_graph
        census = triadic_census(graph, sign_map)
        for (type, count) in census
            counts[type + 1] += census[type]
        end
    end
    triad_count = combinations(s_src, 3) |> length
    return counts ./ N ./ triad_count
end

## Run a number of tests
xs = 0:0.05:1
ys = @showprogress [test_neg_fraction(x) for x in xs]

pyplot()
plot()
for (index, col) in enumerate(zip(ys...) .|> collect)
    plot!(xs, col, label=(index - 1)) |> display
end

## Compare to real graph
first_chunk = first(gdelt_iterator(600_000, 1))
n_f = measure_neg_fraction_in_data(first_chunk)
s_src = 1:188
s_events = 3000
equivalent_n_f_permutation_results = test_neg_fraction(n_f)
graph, source_map, sign_map = compute_agreement_graph(first_chunk)
census = triadic_census(graph, sign_map)
t_c = sum(values(census))
norm_census = Dict(x => y / t_c for (x, y) in census)
println("Census in a chunk of $n_f neg fraction: $census")
println("Census in our graph: $(norm_census)") # Our result
println("Census in a comparable random graph: $(equivalent_n_f_permutation_results)")

