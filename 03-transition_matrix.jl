"""
# 03-transition_matrix.jl

This script drafts the procedure of creating
a transition matrix from consecutive pairs.

It computes 3 three day periods and outputs
a transition matrix computed on joined results.

The result should be in lines of the following.
4×4 Array{Float64,2}:
 0.965293   0.0298463  0.00484601  1.46762e-5
 0.230263   0.704744   0.0642743   0.000718399
 0.0531121  0.131754   0.809076    0.00605773
 0.0136707  0.0750369  0.362295    0.548997

And a pastable representiation can be found below.
[0.965293 0.0298463 0.00484601 1.46762e-5; 0.230263 0.704744 0.0642743 0.000718399; 0.0531121 0.131754 0.809076 0.00605773; 0.0136707 0.0750369 0.362295 0.548997]
"""

import StatsBase.countmap

include("src/sorted_pair.jl")
include("src/agreement.jl")
include("src/triadic_census.jl")
include("src/dataset.jl")
include("src/tests.jl")
include("src/sentiment_dict.jl")

function reverse_triads(triad_collection, source_map)
    reversed_source_map = Dict(map(reverse, source_map |> collect))
    return Set(map(triad -> Set(map(x -> reversed_source_map[x], triad |> collect)),
                   triad_collection |> collect))
end

function compute_chunk(chunk)
    graph, source_map, sign_map = compute_agreement_graph(chunk)
    triad_collection = triads(graph)
    return triad_collection, source_map, sign_map
end

function triad_to_edges(triad, source_map)
    return triad |> collect |>
    t -> map(x -> source_map[x], t) |>
    c -> combinations(c, 2) |>
    collect |> i -> map(SortedPair, i)
end

function check_triad_type(triad, source_map, sign_map)
    edges = triad_to_edges(triad, source_map) |>
            t -> map(x -> sign_map[x], t)
    return 3 - count(edges)
end

function intersection_census(triad_intersection, source_map, sign_map)
    d = Dict{Set{String},Int8}()
    for triad = triad_intersection |> collect
        d[triad] = check_triad_type(triad, source_map, sign_map)
    end
    return d
end

results = []
for chunk_pair = gdelt_iterator_time_pairwise(DateTime(2016, 6, 4), DateTime(2016, 6, 20), Day(3), Day(1))
    println("Processing raw data")
    chunks = chunk_pair .|> compute_chunk
    println("Triad intersection")
    triad_intersection = reverse_triads(chunks[1][1:2]...) ∩ reverse_triads(chunks[2][1:2]...)
    println("Countmap")
    result = zip(intersection_census(triad_intersection, chunks[1][2:3]...) |> values,
                 intersection_census(triad_intersection, chunks[2][2:3]...) |> values) |> collect |> countmap
    println("Appending to results")
    append!(results, [Dict(result)])
    if length(results) == 3
        break
    end
end

d = merge(+, results...)
A = zeros(4, 4)
for key = keys(d)
    A[key.+1...] = d[key]
end
A = A ./ sum(A, dims=2)

