"""
# 04-laplacian_test.jl

This script defines and runs the alternative
Laplacian least eigenvalue test for balance.
"""

using LinearAlgebra
import StatsBase.countmap
import StatsBase.sample
import StatsBase.Weights

include("src/sorted_pair.jl")
include("src/agreement.jl")
include("src/triadic_census.jl")
include("src/dataset.jl")
include("src/tests.jl")
include("src/sentiment_dict.jl")

function lesl(graph, sign_map)
    c = graph |> connected_components |> c -> c[findmax(length.(c))[2]]
    D = diagm(0 => degree(graph))
    n = graph |> vertices |> length
    A = zeros(Int8, n, n)
    for (k, _) = filter((_, v) -> v == false, sign_map)
        k = k |> collect
        if k[1] ∈ c && k[2] ∈ c
            A[k[1], k[2]] = A[k[2], k[1]] = -1
        end
    end
    return eigmin(D - A)
end

function f(k, v)
    if k |> unique |> length == 1
        v = true
    end
    return k => v
end

function lesl_test(graph, sign_map, n_iter)
    original_lesl = lesl(graph, sign_map)
    lesls = []
    for i = 1:n_iter
        random_graph = erdos_renyi(graph |> vertices |> length,
                                   graph |> edges |> length)
        true_rate = (sign_map |> values |> count) /
                    (sign_map |> values |> length) / 100
        E = random_graph |> edges .|> Tuple .|> SortedPair
        tmp = Dict(e => sample([true, false],
                Weights([true_rate, 1-true_rate])) for e in E)
        random_sign_map = Dict(f(k, v) for (k, v) = tmp)
        append!(lesls, lesl(random_graph, random_sign_map))
    end
    return (original_lesl - mean(lesls)) / std(lesls)
end

println("Loading data...")
first_chunk = first(gdelt_iterator(200_000, 1, overlap=0.0))
graph, source_map, sign_map = compute_agreement_graph(first_chunk)

z_score = lesl_test(graph, sign_map, 10)
println("The z-score of Laplacian least eigenvalue test is $(z_score)")

