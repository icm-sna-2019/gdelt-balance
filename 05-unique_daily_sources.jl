"""
# 05-unique_daily_sources.jl

This script identifies unique sources that
are posting daily in given period. This is
useful for identifying sources of interest
in long-term triad tracking.

It computes percentage normalized by the
quantity of existing unique sources.
"""

using Dates

include("src/sorted_pair.jl")
include("src/agreement.jl")
include("src/triadic_census.jl")
include("src/dataset.jl")
include("src/tests.jl")
include("src/sentiment_dict.jl")

date(date) = Dates.Date(date, DATEFORMAT)

function find_everyday_mentioning_sources(chunk)
    df = chunk[[:MentionSourceName, :MentionTimeDate]]
    df.MentionTimeDate = map(x -> date(string(x)), df.MentionTimeDate)
    gb = groupby(df, :MentionTimeDate) |>
         collect .|>
         x -> Set(x.MentionSourceName)
    return intersect(gb...)
end

chunks = [first(gdelt_iterator_time(DateTime(2016, 6, 4),
                                  DateTime(2016, 6, 20),
                                  Day(3),
                                  Day(1)))]

# Unique sources
unique_sources = map(x -> x[:MentionSourceName] |> unique |> Set, chunks) .|> length

# Unique sources posting at least daily
daily_sources = map(find_everyday_mentioning_sources, chunks) .|> length

# Normalized percentage
daily_sources ./ unique_sources .|> x -> round(x, digits=2) |> println
