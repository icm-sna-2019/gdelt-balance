"""
# 06-source_activity_by_chunk.jl

This script identifies activity of sources
per chunk. That is, how many articles they
posted.

See below for length path for DEFULT_* settings:

78 70 69 64 61 56 55 54 52 52 51 51 51

And see below for length path from 4.6.16 to
20.7.16 with width 7 days and stride 3 days:

91 84 81 77 77 74 71
"""

using JLD
using DataFrames
using ProgressMeter

include("src/dataset.jl")

START_DATE = DateTime(2016, 5, 1)
END_DATE = DateTime(2016, 8, 1)
WIDTH = Day(5)
STRIDE = Day(2)

if !("out" in readdir())
    mkdir("out")
end

"""
    occurrence_dict(chunk)

Returns a dictionary of `mention source name` => `number of articles` pairs.
"""
function occurrence_dict(chunk)
    return aggregate(unique(chunk, :MentionIdentifier), :MentionSourceName,
                     length)[[:MentionSourceName, :GlobalEventID_length]] |>
    eachrow .|> (x -> x[:MentionSourceName] => x[:GlobalEventID_length]) |> Dict
end

top_sources_list = Vector{Set{String}}()
@showprogress for chunk in gdelt_iterator_time(START_DATE,
                                               END_DATE,
                                               WIDTH,
                                               STRIDE)
    tmp = occurrence_dict(chunk)
    top_sources = Set(map(y -> y[1], (sort(collect(tmp), by=x -> x[2]) |> reverse)[1:100]))
    print("works")
    push!(top_sources_list, top_sources)
end


for i = 2:3:length(top_sources_list)
    save("out/interim_sources_$(i).jld", "interim_souces_$(i)", intersect(top_sources_list[1:i]...))
end

save("out/chosen_sources.jld", "chosen_sources" intersect(top_sources_list...))
