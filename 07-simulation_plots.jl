"""
# 07-simulation_plots.jl

This script plots null model realizations
as violin plots as an alternative to z-score.

An example from 4/6/2016 to 10/6/2016 with a 3
day windows and 1 day stride is available below.

Any[Any[311707, Any[1044357, 1035195, 1049825, 1042596, 1045496, 1042156, 1040686, 1043298, 1047559, 1044770, 1046107, 1046460, 1046423, 1050666, 1046083, 1043778, 1045854, 1046907, 1050034, 1049983]], Any[419239, Any[1208950, 1212219, 1220064, 1213205, 1219698, 1220783, 1219565, 1220958, 1219013, 1216905, 1215701, 1215613, 1222918, 1214306, 1216379, 1219426, 1218301, 1219583, 1218511, 1213351]], Any[600617, Any[1659290, 1661784, 1657297, 1657667, 1658338, 1665240, 1654857, 1661086, 1652879, 1662193, 1658701, 1662273, 1653197, 1657972, 1659544, 1652521, 1661009, 1660348, 1657650, 1655943]], Any[537139, Any[1452889, 1459325, 1456793, 1461649, 1452750, 1459504, 1460194, 1454514, 1458035, 1456097, 1454324, 1455696, 1460734, 1454917, 1453792, 1453626, 1452971, 1451718, 1457222, 1457007]]]
"""

using StatsPlots
pyplot()
include("src/sorted_pair.jl")
include("src/agreement.jl")
include("src/triadic_census.jl")
include("src/dataset.jl")
include("src/tests.jl")
include("src/sentiment_dict.jl")

results = []
for chunk = gdelt_iterator_time(DateTime(2016, 6, 4), DateTime(2016, 6, 6), Hour(6), Hour(2))
    graph, source_map, sign_map = compute_agreement_graph(chunk)
    triad_collection = triads(graph)
    simulated_censuses = simulate(graph, sign_map, triad_collection, 1, 20)
    original_census = triadic_census(graph, sign_map, triad_collection)[1]
    println(original_census, " ", simulated_censuses |> mean)
    append!(results, [[original_census, simulated_censuses]])
end

violin(hcat(collect(zip(results...))[2]...), leg=false)
scatter!(collect(collect(zip(results...))[1]), leg=false)

