"""
# 08-draft_pipeline.jl

This script contains draft version of the
pipeline which will compute all simulations
needed for the paper over a short period of
time and with little simulations.

It accepts arguments in the following order:
START_DATE, END_DATE, WIDTH, STRIDE, N_ITER

For example the following is a valid call:
julia 08-draft_pipeline.jl 2016 6 10 \\
                           2016 6 30 \\
                           5 \\
                           2 \\
                           100

The results are saved in out directory
that if not there, is created in pwd.
"""

using JLD
using LinearAlgebra
import StatsBase.countmap
import StatsBase.sample
import StatsBase.Weights

include("src/sorted_pair.jl")
include("src/agreement.jl")
include("src/triadic_census.jl")
include("src/dataset.jl")
include("src/tests.jl")
include("src/sentiment_dict.jl")

if !("out" in readdir())
    mkdir("out")
end

#########################
### Laplacian testing ###
#########################

function lesl(graph, sign_map)
    c = graph |> connected_components |> c -> c[findmax(length.(c))[2]]
    D = diagm(0 => degree(graph))
    n = graph |> vertices |> length
    A = zeros(Int8, n, n)
    for (k, _) = filter((_, v) -> v == false, sign_map)
        k = k |> collect
        if k[1] ∈ c && k[2] ∈ c
            A[k[1], k[2]] = A[k[2], k[1]] = -1
        end
    end
    return eigmin(D - A)
end

function f(k, v)
    if k |> unique |> length == 1
        v = true
    end
    return k => v
end

function lesl_simulate(graph, sign_map, n_iter)
    lesls = []
    for i = 1:n_iter
        random_graph = erdos_renyi(graph |> vertices |> length,
                                   graph |> edges |> length)
        true_rate = (sign_map |> values |> count) /
                    (sign_map |> values |> length) / 100
        E = random_graph |> edges .|> Tuple .|> SortedPair
        tmp = Dict(e => sample([true, false],
                Weights([true_rate, 1-true_rate])) for e in E)
        random_sign_map = Dict(f(k, v) for (k, v) = tmp)
        append!(lesls, lesl(random_graph, random_sign_map))
    end
    return lesls
end

#########################
### Transition matrix ###
#########################

function reverse_triads(triad_collection, source_map)
    reversed_source_map = Dict(map(reverse, source_map |> collect))
    return Set(map(triad -> Set(map(x -> reversed_source_map[x],
                                    triad |> collect)),
                   triad_collection |> collect))
end

function compute_chunk(chunk)
    graph, source_map, sign_map = compute_agreement_jaccard_graph(chunk)
    triad_collection = triads(graph)
    return triad_collection, source_map, sign_map
end

function triad_to_edges(triad, source_map)
    return triad |> collect |>
    t -> map(x -> source_map[x], t) |>
    c -> combinations(c, 2) |>
    collect |> i -> map(SortedPair, i)
end

function check_triad_type(triad, source_map, sign_map)
    edges = triad_to_edges(triad, source_map) |>
            t -> map(x -> sign_map[x], t)
    return 3 - count(edges)
end

function intersection_census(triad_intersection, source_map, sign_map)
    d = Dict{Set{String},Int8}()
    for triad = triad_intersection |> collect
        d[triad] = check_triad_type(triad, source_map, sign_map)
    end
    return d
end

###########################
### Triadic simulations ###
###########################

function triad_simulate(graph, sign_map, triad_collection, n_iter)
    censuses = []
    for i = 1:n_iter
        census = triadic_census(graph, shuffle_signs(sign_map), triad_collection)
        append!(censuses, census)
    end
    return censuses
end

################
### Pipeline ###
################

i = 0
results = []
if ARGS == []
    config = [DateTime(2016, 6, 4), DateTime(2016, 6, 14), Day(3), Day(1)]
elseif length(ARGS) != 9
    println("Invalid dimensions of input. Need START_DATE, END_DATE, WIDTH, STRIDE, N_ITER")
    exit()
else
    config = [
              DateTime(map(x -> parse(Int, x), ARGS[1:3])...),
              DateTime(map(x -> parse(Int, x), ARGS[4:6])...),
              Day(parse(Int, ARGS[7])),
              Day(parse(Int, ARGS[8]))
             ]
end
filename = "out/" * Dates.format(Dates.now(), "dd-mm-yyyy_HH:MM_") *
    reduce(*, map(x -> Dates.format(x, "dd-mm-yyyy_"), config[1:2])) *
    string(config[3].value) *
    "_" * string(config[4].value) * "_"
for chunk_pair = gdelt_iterator_time_pairwise(config...)
    global i += 1
    println("Chunk pair $(i)")
    ch1, ch2 = chunk_pair
    println("Making agreement graph 1 (left)")
    graph1, source_map1, sign_map1 = compute_agreement_jaccard_graph(ch1)
    triad_collection1 = triads(graph1)

    println("Simulating by sign permutation in graph 1 (left)")
    triadic_simulation = triad_simulate(graph1, sign_map1, triad_collection1, parse(Int, ARGS[end]))
    append!(triadic_simulation, triadic_census(graph1, sign_map1, triad_collection1))
    save(filename * "triad_sim_pair$(i).jld", "triadic_simulation", triadic_simulation)
    triadic_simulation = nothing

    println("Simulating Erdos-Renyi from graph 1 (left)")
    lesl_simulation = lesl_simulate(graph1, sign_map1, parse(Int, ARGS[end]))
    append!(lesl_simulation, lesl(graph1, sign_map1))
    save(filename * "lesl_sim_pair$(i).jld", "lesl_simulation", lesl_simulation)
    graph1 = nothing
    lesl_simulation = nothing

    println("Making agreement graph 2")
    graph2, source_map2, sign_map2 = compute_agreement_jaccard_graph(ch2)
    triad_collection2 = triads(graph2)
    graph2 = nothing

    println("Intersecting both graphs")
    triad_intersection = reverse_triads(triad_collection1, source_map1) ∩
                         reverse_triads(triad_collection2, source_map2)
    triad_collection1 = nothing
    triad_collection2 = nothing

    println("Zipping and counting intersections")
    transitions = zip(intersection_census(triad_intersection,
                                          source_map1,
                                          sign_map1) |> values,
                      intersection_census(triad_intersection,
                                          source_map2,
                                          sign_map2) |> values) |> collect |> countmap
    triad_intersection = nothing
    source_map1 = nothing
    source_map2 = nothing
    sign_map1 = nothing
    sign_map2 = nothing
    save(filename * "transitions_pair$(i).jld", "transitions", transitions)
    append!(results, [Dict(transitions)])
end

d = merge(+, results...)
A = zeros(4, 4)
for key = keys(d)
    A[key.+1...] = d[key]
end
A = A ./ sum(A, dims=2)
save(filename * "transition_matrix.jld", "A", A)

