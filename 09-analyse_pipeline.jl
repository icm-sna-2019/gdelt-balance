"""
# 09-analyse_pipeline.jl

This script will produce three graphs
from the results of draft pipeline:
1. Violin plots of Laplacian eigenvalue
2. Violin plots of triads of type 1
3. Heatmap of transition matrix

This needs to be run in folder with
only the .jld files from experiments.
"""

using JLD
using StatsPlots
gr()

if !("plots" in readdir())
    mkdir("plots")
end

lesls = []
_triadic = []
transitions = []
matrix = [1 0 0 0; 0 1 0 0; 0 0 1 0; 0 0 0 1]
for path in readdir(".")
    if occursin("lesl", path)
        append!(lesls, [collect(load(path))[1][2]])
    elseif occursin("triad", path)
        append!(_triadic, [collect(load(path))[1][2]])
    elseif occursin("trans", path) & !occursin("matrix", path)
        append!(transitions, [collect(load(path))[1][2]])
    elseif occursin("trans", path) & occursin("matrix", path)
        global matrix = collect(load(path))[1][2]
    end
end

# Plotting least Laplacian eigenvalue test
lesl_results = [[lesls[i][end], lesls[i][1:end-1]] for i in 1:length(lesls)]
violin(hcat(collect(zip(lesl_results...))[2]...), leg=false, grid=false)
scatter!(collect(collect(zip(lesl_results...))[1]), leg=false, grid=false)
xlabel!("Least signed Laplacian eigenvalue")
savefig("plots/lesl.png")

# Divide by number of triads in chunks
triadic = []
t = []
for _t in _triadic
    for i in 1:4:length(_t)
        tmp = zip([0, 2, 3, 1], map(x -> x[2], _t[i:i+3]) ./ (map(x -> x[2], _t[i:i+3]) |> sum)) |> Dict |> collect
        append!(t, tmp)
    end
    append!(triadic, [t])
    global t = []
end

# Function for plotting triadic test
function plot_triad(triad_type)
    triadic_results = [[map(y -> y[2], filter(x -> x[1] == triad_type, triadic[i]))[end],
                        map(y -> y[2], filter(x -> x[1] == triad_type, triadic[i]))[1:end-1]]
                       for i in 1:length(triadic)]
    violin(hcat(collect(zip(triadic_results...))[2]...), leg=false, grid=false)
    scatter!(collect(collect(zip(triadic_results...))[1]), leg=false, grid=false)
    xlabel!("Triads of type $triad_type")
end

# Plotting triadic census test for triads of type 1
plot_triad(0); savefig("plots/type0.png")
plot_triad(1); savefig("plots/type1.png")
plot_triad(2); savefig("plots/type2.png")
plot_triad(3); savefig("plots/type3.png")

# Plotting transition matrix
heatmap([0,1,2,3], [0,1,2,3], matrix)
xlabel!("Row-stochastic transition matrix for triad types")
savefig("plots/matrix.png")

