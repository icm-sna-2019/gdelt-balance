"""
# 10-plot_pipeline.jl

This script traverses longer timeframe and
1) measures balance by means of census and LESL
2) builds and plots graph of chosen sources

"""

using JLD
using LinearAlgebra
using GraphPlot
using Dates
using Colors
using Compose
using DelimitedFiles

include("src/sorted_pair.jl")
include("src/agreement.jl")
include("src/triadic_census.jl")
include("src/dataset.jl")
include("src/tests.jl")
include("src/sentiment_dict.jl")

START_DATE = DateTime(2016, 5, 1)
END_DATE = DateTime(2016, 8, 1)
WIDTH = Day(5)
STRIDE = Day(2)

if !("out" in readdir())
    mkdir("out")
end

function lesl(graph, sign_map)
    c = graph |> connected_components |> c -> c[findmax(length.(c))[2]]
    D = diagm(0 => degree(graph))
    n = graph |> vertices |> length
    A = zeros(Int8, n, n)
    for (k, _) = filter((_, v) -> v == false, sign_map)
        k = k |> collect
        if k[1] ∈ c && k[2] ∈ c
            A[k[1], k[2]] = A[k[2], k[1]] = -1
        end
    end
    return eigmin(D - A)
end

function plot_graph(g, source_map, sign_map)
    posc = parse(Colorant, RGBA(0, 0, 255, 0.2))
    negc = colorant"red"
    rev_source_map = Dict(map(reverse, source_map |> collect))
    nodelabel = map(x -> rev_source_map[x], vertices(g) |> collect)
    edgestrokec = map(x -> sign_map[SortedPair(x.src, x.dst)] ? posc : negc , edges(g) |> collect)
    nodesize = [outdegree(g, v) for v in vertices(g)]
    gplot(g, layout=circular_layout, nodelabel=nodelabel, edgestrokec=edgestrokec, nodesize=nodesize)
end

chosen_sources = Set(["independent.co.uk", "investegate.co.uk", "eveningexpress.co.uk", "thenorthernecho.co.uk", "belfasttelegraph.co.uk", "telegraph.co.uk", "express.co.uk", "bbc.com", "dailyrecord.co.uk", "standard.co.uk", "dailymail.co.uk", "mirror.co.uk", "dailystar.co.uk", "theguardian.com", "huffingtonpost.co.uk", "metro.co.uk", "scotsman.com"])

fmt(date) = Dates.format(date, "dd-mm-yy")

open("out/censuses.txt", "a") do io
    writedlm(io, [0 1 2 3 "lesl"])
end

i = 0
for chunk in gdelt_iterator_time(START_DATE, END_DATE, WIDTH, STRIDE)
    global i += 1
    if size(chunk)[1] == 0
        continue
    end
    # Building graph with full data
    graph, source_map, sign_map = compute_agreement_jaccard_graph(chunk)
    triad_collection = triads(graph)
    census = triadic_census(graph, sign_map, triad_collection)
    open("out/censuses.txt", "a") do io
        writedlm(io, [census[0] census[1] census[2] census[3] lesl(graph, sign_map)])
    end
    graph = nothing
    source_map = nothing
    sign_map = nothing
    triad_collection = nothing
    # Repeating for chosen sources
    filtered_chunk = filter(x -> x[:MentionSourceName] ∈ chosen_sources, chunk)
    graph, source_map, sign_map = compute_agreement_jaccard_graph(filtered_chunk)
    title = "out/chunk_$(i)_from_" *
            fmt(START_DATE + i * WIDTH - i * STRIDE) *
            "_to_" *
            fmt(START_DATE + (i + 1) * WIDTH - i * STRIDE)
    save("$(title).jld", "graph", graph, "source_map", source_map, "sign_map", sign_map)
    draw(SVG("$(title).svg", 20cm, 20cm), plot_graph(graph, source_map, sign_map))
end

