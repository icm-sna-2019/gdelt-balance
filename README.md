# gdelt-balance
This repository contains scripts for our media structural balance in GDELT project.

## Usage
First download a data sample by running
```
bash 00-download_sample.sh
```

Make sure you have these Julia packages installed:
 - `DataFrames`
 - `Combinatorics`
 - `LightGraphs`
 - `CSV`
 - `ProgressMeter`
 - `Statistics`

Having done so, simply execute
```
julia 01-balance_test.jl
```
or run the file's contents in the REPL.

## Tests
To run tests, execute
```
julia test/runtests.jl
```
