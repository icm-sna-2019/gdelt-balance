using LightGraphs
using DataFrames: groupby, by
using Combinatorics: combinations
using StatsBase: mean

"""
    compute_agreement(data)

Computes agreement between sources in the given GDELT mentions chunk,
and returns a dict of `ordered mention source name tuple: agreement`
pairs, such that agreement is defined by the number of events that their
mention doc tone agrees on.
"""
function compute_agreement(data)
    agreement_dict = Dict{SortedPair{String}, Float64}()
    for group = groupby(data, :GlobalEventID)
        # Get rid of duplicates from within this data chunk
        # by setting their doc tone as the mean
        group = by(group, :MentionSourceName, :MentionDocTone => mean)
        for pair = combinations(eachrow(group), 2)
            tuples = map(x -> Tuple(x[[:MentionSourceName, :MentionDocTone_mean]]), pair)
            sources, sentiments = map(collect, zip(tuples...))
            sources = SortedPair(sources[1], sources[2])
            if !haskey(agreement_dict, sources)
                agreement_dict[sources] = 0
            end
            agreement_dict[sources] += sign(prod(sentiments))
        end
    end
    return agreement_dict
end

"""
    jaccard(s1::Set, s2::Set)::Float64

Computes the Jaccard coefficient between two sets, defined as the size of the
intersection divided by the size of the union of the sets.
"""
function jaccard(s1::Set, s2::Set)::Float64
    return length(intersect(s1, s2))/length(union(s1, s2))
end

"""
    compute_agreement_jaccard(data)

Computes agreement between sources in the given GDELT mentions chunk, 
and returns a dict of `ordered mention source name tuple: agreement`
pairs, such that agreement is defined as the weighted sum of article pairs
between two sources with the weight given by the Jaccard coefficient of
overlapping events mentioned and with the positive/negative sign decided by
whether their sentiments match.
"""
function compute_agreement_jaccard(data)
    agreement_dict = Dict{SortedPair{String}, Float64}()
    # for quick reference as to which events are mentioned in an article, its
    # sentiment and all articles with which it was found to overlap before such
    # that we do not compute the Jaccard coeff again
    article_dict = Dict{String, Tuple{Set{Int64}, Float64, Set{String}}}()
    for article = groupby(data, :MentionIdentifier)
        article_name = first(article[:MentionIdentifier])
        event_set = Set(article[:GlobalEventID])
        sentiment = first(article[:MentionDocTone]) # all of the values are the same
        article_dict[article_name] = (event_set, sentiment, Set{String}())
    end
    for event = groupby(data, :GlobalEventID)
        for pair = combinations(eachrow(event), 2)
            S1, S2 = [r[:MentionSourceName] for r in pair]
            # Do not look at agreement within a single source
            if S1 == S2
                continue
            end
            A1, A2 = [r[:MentionIdentifier] for r in pair]
            # Has this pair of articles been computed before?
            if A2 ∈ article_dict[A1][3]
                continue
            end
            coeff = (jaccard(article_dict[A1][1], article_dict[A2][1])
                     * sign(article_dict[A1][2] * article_dict[A2][2]))
            sources = SortedPair(S1, S2)
            if !haskey(agreement_dict, sources)
                agreement_dict[sources] = 0
            end
            agreement_dict[sources] += coeff
            # Blacklist this pair from being recomputed down the road
            push!(article_dict[A1][3], A2)
            push!(article_dict[A2][3], A1)
        end
    end
    return agreement_dict
end

"""
    dict_to_graph(data)

Transforms an agreement dict into a graph.
"""
function dict_to_graph(agreement_dict)
    unique_sources = Set(collect(Iterators.flatten(collect(keys(agreement_dict)))))
    source_map = Dict(i[2] => i[1] for i in enumerate(unique_sources))
    sign_map = Dict(map(x -> source_map[x], i) |> collect |> SortedPair => agreement_dict[i] < 0 ? false : true for i in keys(agreement_dict))
    graph = SimpleGraph(source_map |> collect |> length)
    for key = keys(agreement_dict)
        add_edge!(graph, map(x -> source_map[x], key))
    end
    return graph, source_map, sign_map
end

"""
    compute_agreement_graph(data)

Computes agreement graph.

Returns the graph, a dictionary mapping sources to node numbers and dictionary
mapping node number pair tuples to signs of their edge.
"""
function compute_agreement_graph(data)
    agreement_dict = compute_agreement(data)
    return dict_to_graph(agreement_dict)
end

"""
    compute_agreement_jaccard_graph(data)

Computes agreement graph using the Jaccard coefficient agreement method.

Returns the graph, a dictionary mapping sources to node numbers and dictionary
mapping node number pair tuples to signs of their edge.
"""
function compute_agreement_jaccard_graph(data)
    agreement_dict = compute_agreement_jaccard(data)
    return dict_to_graph(agreement_dict)
end
