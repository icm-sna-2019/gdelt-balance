import CSV
import Base: iterate, length
using DataFrames: dropmissing, filter, DataFrame
using Dates

# Experiment constants
DEFAULT_START_DATE = DateTime(2016, 6, 4)
DEFAULT_END_DATE = DateTime(2016, 6, 20)
DEFAULT_WIDTH = Day(3)
DEFAULT_STRIDE = Day(1)

## Data metainformation
CONFIDENCE_CUTOFF = 40
SENTIMENT_CUTOFF = 0.01
DATA_PATH = "./dataset/mentions.csv"
DATA_COLUMNS = [:GlobalEventID, :EventTimeDate, :MentionTimeDate, :MentionType, :MentionSourceName, :MentionIdentifier, :SentenceID, :Actor1CharOffset, :Actor2CharOffset, :ActionCharOffset, :InRawText, :Confidence,:MentionDocLen, :MentionDocTone, :MentionDocTranslationInfo, :Extras]
DATA_TYPES = Dict(
  :InRawText                 => Int64,
  :MentionTimeDate           => Int64,
  :MentionType               => Int64,
  :MentionDocTone            => Float64,
  :Actor2CharOffset          => Int64,
  :MentionIdentifier         => Union{Missing, String},
  :SentenceID                => Int64,
  :MentionSourceName         => Union{Missing, String},
  :Extras                    => Union{Missing, String},
  :ActionCharOffset          => Int64,
  :Confidence                => Int64,
  :Actor1CharOffset          => Int64,
  :MentionDocTranslationInfo => Union{Missing, String},
  :GlobalEventID             => Int64,
  :MentionDocLen             => Int64,
  :EventTimeDate             => Int64)
SOURCES_BY_COUNTRY_PATH = "./dataset/sourcesbycountry.csv"
SOURCES_BY_COUNTRY_COLUMNS = [:MentionSourceName, :CountryCode, :CountryName]

sources_by_country = CSV.read(SOURCES_BY_COUNTRY_PATH, header=SOURCES_BY_COUNTRY_COLUMNS, delim="\t")
allowed_sources = Set(sources_by_country[sources_by_country.CountryCode .== "UK", :][:MentionSourceName])

"""
    clean(data)

Returns a clean dataframe, without missing source names or non-UK sources.
"""
function clean(data)
    data = dropmissing(data, :MentionSourceName)
    data = filter(data -> data.MentionSourceName ∈ allowed_sources, data)
    data = filter(data -> abs(data.MentionDocTone) >= SENTIMENT_CUTOFF, data)
    data = filter(data -> data.Confidence >= CONFIDENCE_CUTOFF, data)
    return data
end

"""
    load_chunk(first_row, row_count)

Helper function that cleans and returns a chosen chunk of GDELT mentions.
"""
function load_chunk(first_row, row_count)
    return CSV.read(DATA_PATH, skipto=first_row, limit=row_count,
                    header=DATA_COLUMNS) |> clean
end

## Line number based chunking
"""
    gdelt_iterator(chunk_size::Int64, total_chunks::Int64; overlap::Float64=0.0)

Returns an iterator that returns chunks of the mentions table.

Overlap is an optional argument that defines what fraction of the previous
chunk should be considered in each chunk. Should be float x ∈ [0.0, 1.0)
"""
function gdelt_iterator(chunk_size::Int64, total_chunks::Int64; overlap=0.0::Real)
    step_size = chunk_size * (1 - overlap)
    range_end = step_size * total_chunks + 1
    return (CSV.read(DATA_PATH, skipto=first_row, limit=chunk_size,
                     header=DATA_COLUMNS) |> clean for first_row in
            1:Int64(step_size):Int64(range_end))
end

## Time based chunking
DATEFORMAT = dateformat"yyyymmddHHMMSS"
date_to_str(date) = Dates.format(date, DATEFORMAT)
date_to_str(date::Date) = date_to_str(convert(DateTime, date))
date_to_int(date) = parse(Int64, date_to_str(date))

"""
    GDELTDateTimeSlicer

A stateful iterator that divides the data into chunks of equal duration.

It returns tuples of integer values, such that the first designates when a
chunk starts and the second how long it is. These can then be used as `skipto`
and `limit` arguments of CSV.File
"""
mutable struct GDELTDateTimeSlicer
    t_0::DateTime
    t_f::DateTime
    t_f_str::String
    width::Period
    stride::Period
    step::Int64
    cursor_index::Int64
    step_begin_indices::Vector{Int64} # Indices of the beginning of each chunk
    step_begin_indices_found::Int64 # How many of the above have we found so far?
    rows::Base.Iterators.Stateful # Rows instance from DataFrames to iterate over

    function GDELTDateTimeSlicer(t_0::DateTime, t_f::DateTime, width::Period, stride::Period, rows::Base.Iterators.Stateful)
        return new(
                  t_0,
                  t_f,
                  date_to_str(t_f),
                  width,
                  stride,
                  0,
                  0,
                  Vector{Int64}(),
                  0,
                  rows)
    end
end

function iterate(dt_slicer::GDELTDateTimeSlicer, state = 0)
    if state == -1
        return nothing
    end
    next_begin = dt_slicer.t_0 + (dt_slicer.step_begin_indices_found * dt_slicer.stride)
    curr_begin = dt_slicer.t_0 + (dt_slicer.step * dt_slicer.stride)
    curr_end = curr_begin + dt_slicer.width

    next_begin_str = date_to_str(next_begin)
    curr_end_str = date_to_str(curr_end)

    for row in dt_slicer.rows
        dt_slicer.cursor_index += 1
        if row.MentionTimeDate >= next_begin_str
            push!(dt_slicer.step_begin_indices, dt_slicer.cursor_index)
            dt_slicer.step_begin_indices_found += 1
            next_begin = dt_slicer.t_0 + (dt_slicer.step_begin_indices_found * dt_slicer.stride)
            next_begin_str = date_to_str(next_begin)
        end
        if row.MentionTimeDate >= dt_slicer.t_f_str
            break
        end
        if row.MentionTimeDate >= curr_end_str
            begin_index = popfirst!(dt_slicer.step_begin_indices)
            dt_slicer.step += 1
            return ((begin_index, dt_slicer.cursor_index - begin_index), dt_slicer.step)
        end
    end
    dt_slicer.step = -1
    return ((dt_slicer.step_begin_indices[1], dt_slicer.cursor_index - dt_slicer.step_begin_indices[1]), -1)
end

function _compound_period_to_minutes(cp::Dates.CompoundPeriod)::Minute
    return sum(cp.periods .|> x -> convert(Minute, x))
end

function length(dt_slicer::GDELTDateTimeSlicer)::Int64
    k = Int64(ceil(_compound_period_to_minutes(dt_slicer.t_f - dt_slicer.t_0 - dt_slicer.width) /
                   convert(Minute, dt_slicer.stride)))
    k = max(k, 0)
    return k + 1
end

"""
    gdelt_slicer_time(t_0::DateTime, t_f::DateTime, width::Period, stride::Period)

Returns an iterator that returns tuples describing row indices of chunks of
given time width and stride. Each tuple is of form `(starting_row, number_of_rows)`
"""
function gdelt_slicer_time(t_0::DateTime, t_f::DateTime, width::Period, stride::Period)
    rows = CSV.Rows(DATA_PATH, header=DATA_COLUMNS, reusebuffer=true)
    rows_st = Base.Iterators.Stateful(rows)
    return GDELTDateTimeSlicer(t_0, t_f, width, stride, rows_st)
end

"""
    gdelt_iterator_time(t_0::DateTime, t_f::DateTime, width::Period, stride::Period)

Returns an iterator that returns chunks of the mentions table with given time
width and stride.

For example
```
julia> for data in gdelt_iterator_time(DateTime(2016, 5, 3), DateTime(2016, 5, 3, 15), Hour(3), Hour(1))
           display(data)
       end
```
will result in 13 chunks, each 3 hours wide with 1 hour step between each consequent pair.
"""
function gdelt_iterator_time(t_0::DateTime, t_f::DateTime, width::Period, stride::Period)
    return (CSV.read(DATA_PATH, skipto=first_row, limit=row_count,
                     header=DATA_COLUMNS) |> clean for (first_row, row_count) in
            gdelt_slicer_time(t_0, t_f, width, stride))
end

"""
    get_nth_chunk(n::Integer, t_0::DateTime, t_f::DateTime, width::Period, stride::Period)

Returns `n`th chunk as given by a slicer with these parameters.
"""
function get_nth_chunk(n::Integer, t_0::DateTime, t_f::DateTime, width::Period, stride::Period)
    slicer = gdelt_slicer_time(t_0, t_f, width, stride)
    for (iter, (first_row, row_count)) in enumerate(slicer)
        if iter == n
            return CSV.read(DATA_PATH, skipto=first_row, limit=row_count,
                            header=DATA_COLUMNS) |> clean
        end
    end
    throw(BoundsError(n, "Chunk number too great for dataset"))
    return get_nth_chunk(n, slicer)
end

"""
    GDELTPairwiseChunkIterator

A stateful iterator that returns pairs of chunks, loading each chunk into
memory only once. It accepts the same parameters as GDELTDateTimeSlicer.
"""
mutable struct GDELTPairwiseChunkIterator
    c_1
    c_2
    slicer::GDELTDateTimeSlicer

    function GDELTPairwiseChunkIterator(t_0, t_f, width, stride, rows)
        return new(nothing,
                   nothing,
                   GDELTDateTimeSlicer(t_0, t_f, width, stride, rows))
    end
end

function iterate(p_iterator::GDELTPairwiseChunkIterator, state = true)
    if !state
        return nothing
    end

    if p_iterator.c_1 == nothing
        (first_row, row_count), slicer_state = iterate(p_iterator.slicer)
        p_iterator.c_1 = load_chunk(first_row, row_count)
    else
        p_iterator.c_1 = p_iterator.c_2
    end
    # The slicer is stateful, so we don't need to provide state to iterate at this point
    (first_row, row_count), slicer_state = iterate(p_iterator.slicer)
    p_iterator.c_2 = load_chunk(first_row, row_count)
    return ((p_iterator.c_1, p_iterator.c_2), slicer_state != -1 ? true : false)
end

"""
    gdelt_iterator_time_pairwise(t_0::DateTime, t_f::DateTime, width::Period, stride::Period)

Returns an iterator that returns pairs of chunks of the mentions table with
given time width and stride. Similar to gdelt_iterator_time, but returns tuples
of consecutive pairs of chunks.

For example
```
julia> for (c_1, c_2) in gdelt_iterator_time_pairwise(DateTime(2016, 5, 3), DateTime(2016, 5, 7), Day(2), Day(1))
           println("Another pair")
           display(c_1)
           display(c_2)
       end
```
"""
function gdelt_iterator_time_pairwise(t_0::DateTime, t_f::DateTime, width::Period, stride::Period)
    rows = CSV.Rows(DATA_PATH, header=DATA_COLUMNS, reusebuffer=true)
    rows_st = Base.Iterators.Stateful(rows)
    return GDELTPairwiseChunkIterator(t_0, t_f, width, stride, rows_st)
end
