# sentiment_dict.jl
# This file contains methods for creating and operating on sentiment dicts that
# define a bimodal `(outlet, event) => sentiment` network
#
# Note that these methods are used to analyse low-level behaviour of our model.
# In practice, we do not need to define them directly and can instead compute
# agreement from the data frame, only using them implicitly.

using DataFrames: by
using StatsBase: mean
using Combinatorics: combinations

"""
    sentiment_dict_random(set_src, set_events, frac::Real)

Returns a fully connected weighted bimodal sentiment graph, with `frac`
fraction of negative connections.
"""
function sentiment_dict_random(set_src, set_events, frac::Real)
    sentiments = Dict{Tuple{typeof(first(set_src)), typeof(first(set_events))}, Real}()
    for src in s_src
        for event in s_events
            sentiments[(src, event)] = rand() > frac ? 1 : -1
        end
    end
    return sentiments
end

"""
    sentiment_dict_gdelt(data)

Returns a dictionary of `(source, event_id) => mention doc tone mean` pairs.
The mean is necessary when a source mentions an event multiple times in the
data chunk.
"""
function sentiment_dict_gdelt(data)
    data_unduplicated = by(data, [:GlobalEventID, :MentionSourceName], :MentionDocTone => mean)
    return Dict((row[:MentionSourceName], row[:GlobalEventID]) => row[:MentionDocTone_mean]
                for row in eachrow(data_unduplicated[[:MentionSourceName, :GlobalEventID, :MentionDocTone_mean]]))
end

## Functions to compute source agreement dict from source-event sentiment dict

### To be used when `sentiment_dict` is a complete bimodal network
function _sentiment_dict_to_agreement_complete(sentiment_dict, s_sources, s_events)
    agreement_dict = Dict{SortedPair{eltype(s_sources)}, Float64}()
    s_sources = collect(s_sources) # to be able to use combinations later
    for event in s_events # Group by events
        for pair in combinations(s_sources, 2)
            tuples = map(x -> (x, sentiment_dict[(x, event)]), pair)
            sources, sentiments = map(collect, zip(tuples...))
            sources = SortedPair(sources[1], sources[2])
            if !haskey(agreement_dict, sources)
                agreement_dict[sources] = 0
            end
            agreement_dict[sources] += sign(prod(sentiments))
        end
    end
    return agreement_dict
end

### To be used when `sentiment_dict` is an incomplete bimodal network
function _sentiment_dict_to_agreement_incomplete(sentiment_dict, s_sources, s_events)
    type_sources = eltype(s_sources)
    type_events = eltype(s_events)
    sources_of_events = Dict{type_events, Set{type_sources}}()
    for (src, event) in keys(sentiment_dict)
        if !haskey(sources_of_events, event)
            sources_of_events[event] = Set{type_sources}()
        end
        push!(sources_of_events[event], src)
    end
    # convert to array so that we can iterate over combinations later
    sources_of_events = Dict(p[1] => collect(p[2]) for p in sources_of_events)
    agreement_dict = Dict{SortedPair{type_sources}, Float64}()
    for (event, sources_of_event) in sources_of_events
        for pair in combinations(sources_of_event, 2)
            tuples = map(x -> (x, sentiment_dict[(x, event)]), pair)
            sources, sentiments = map(collect, zip(tuples...))
            sources = SortedPair(sources[1], sources[2])
            if !haskey(agreement_dict, sources)
                agreement_dict[sources] = 0
            end
            agreement_dict[sources] += sign(prod(sentiments))
        end
    end
    return agreement_dict
end

"""
    sentiment_dict_to_agreement(sentiment_dict::Dict; is_certainly_complete=false)

Returns an agreement dict out of a sentiment dict. If `is_certainly_complete`
is true, then the graph will not be tested for completeness and the algorithm
for a complete sentiment dict will be used.
"""
function sentiment_dict_to_agreement(sentiment_dict::Dict; is_certainly_complete=false)
    s_sources, s_events = [Set(getindex.(keys(sentiment_dict), i)) for i in (1, 2)]
    type_sources = eltype(s_sources)
    type_events = eltype(s_events)
    # If the bimodal graph is complete, then we can simply iterate over all pairs of elements
    # otherwise, we need to group them per event smartly
    complete = is_certainly_complete ? true : (length(s_sources) * length(s_events) == length(sentiment_dict));
    if complete
        return _sentiment_dict_to_agreement_complete(sentiment_dict, s_sources, s_events)
    else
        return _sentiment_dict_to_agreement_incomplete(sentiment_dict, s_sources, s_events)
    end
end
