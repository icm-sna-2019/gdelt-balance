import Base: iterate, length, map

"""
    SortedPair{T<:Any}

A type representing a sorted pair, such that its hash is equivalent
regardless of input order.

It is used to uniquely identify pairs of items.
"""
struct SortedPair{T<:Any}
    a::T
    b::T

    function SortedPair{T}(a, b) where {T<:Any}
        if T<:Real || T<:String
            return isless(a, b) ? new{T}(a, b) : new{T}(b, a)
        elseif hasmethod(isless, (T, T))
            return isless(a, b) ? new{T}(a, b) : new{T}(b, a)
        else
            return isless(hash(a), hash(b)) ? new{T}(a, b) : new{T}(b, a)
        end
    end
end

SortedPair(a, b) = SortedPair{typeof(a)}(a, b)
SortedPair(t::Tuple{Any, Any}) = SortedPair(t...)
SortedPair(a::Array) = SortedPair(a...) # assuming shape (2,)

# Interfaces
function iterate(s::SortedPair, i::Int=0)
    if i == 0
        return (s.a, 1)
    elseif i == 1
        return (s.b, 2)
    else
        return nothing
    end
end
length(s::SortedPair) = 2
map(f, s::SortedPair) = (f(s.a), f(s.b))

# Helper aliases
sp(a, b) = SortedPair(a, b)
SP = SortedPair
