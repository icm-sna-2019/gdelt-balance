import Random.shuffle
import Statistics.mean
import Statistics.std
using ProgressMeter

"""
    shuffle_signs(s)

Returns a randomly reshuffled dictionary.
"""
function shuffle_signs(s)
    return Dict(zip(first.([(x, s[x]) for x in keys(s)]), shuffle(last.([(x, s[x]) for x in keys(s)]))))
end


"""
    simulate(graph, sign_map, triad_collection, triad_type, n_iter)

Simulates a single graph. Returns list of census dictionaries.
"""
function simulate(graph, sign_map, triad_collection, triad_type, n_iter)
    censuses = []
    @showprogress 1 "Simulating graphs" for i = 1:n_iter
        census = triadic_census(graph, shuffle_signs(sign_map), triad_collection)
        append!(censuses, census[triad_type])
    end
    return censuses
end


"""
    test(graph, sign_map, triad_collection, triad_type, n_iter)

Returns z-score (number of standard deviations from the mean) for a given triad type (0, 1, 2 or 3).
"""
function test(graph, sign_map, triad_collection, triad_type, n_iter)
    original_census = triadic_census(graph, sign_map, triad_collection)[triad_type]
    censuses = simulate(graph, sign_map, triad_collection, triad_type, n_iter)
    z_score = (original_census - mean(censuses)) / std(censuses)
    return z_score
end
