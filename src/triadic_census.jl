using LightGraphs: neighbors, nv, AbstractGraph

## Triad detection
"""
    triads(g::AbstractGraph)

Get a set of fully connected triads out of this graph.

Each triad is a set of integers, describing nodes.
"""
function triads(g::AbstractGraph; isordered::Bool = true)
    triads = Set{Set{Int64}}()
    vertices = 1:nv(g)
    for v = vertices
        v_neigh = neighbors(g, v)
        for u = v_neigh
            if u < v
                continue
            end
            u_neigh = neighbors(g, u)
            common = intersect(v_neigh, u_neigh)
            for w = common
                if w < u
                    continue
                end
                push!(triads, Set([u, v, w]))
            end
        end
    end
    return triads
end

## Census computation
"""
    triadic_census(g::AbstractGraph, signs::Array{Bool, 1}, triad_collection = nothing)

Returns a mapping of `triad type: number of triads`.

`triad_collection` is an optional precomputed triad collection that will otherwise
be found during execution.
"""
function triadic_census(g::AbstractGraph, sign_d::Dict{SortedPair{Int64}, Bool}, triad_collection = nothing)
    triad_types = Dict(i => 0 for i = 0:3)
    if typeof(triad_collection) <: Nothing
        triad_collection = triads(g)
    end
    for triad = triad_collection
        negative = 3
        for pair = combinations(triad |> collect, 2) .|> SortedPair
            negative -= sign_d[pair]
        end
        triad_types[negative] += 1
    end
    return triad_types
end
