@test date_to_str(DateTime(2016, 6, 17, 18, 15)) == "20160617181500"
@test date_to_str(Date(2016, 6, 17)) == "20160617000000"
@test date_to_int(DateTime(2016, 6, 17, 18, 15)) == 20160617181500
