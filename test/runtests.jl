using Test

# Modules
include("../src/sorted_pair.jl")
include("../src/agreement.jl")
include("../src/triadic_census.jl")
include("../src/dataset.jl")
include("../src/tests.jl") # shuffle_signs
include("../src/sentiment_dict.jl")

# Tests
include("sentiment_dict.jl")
include("dataset.jl")
