"""
    evaluate_sentiment_dict_accuracy()

Helper function that compares results of census calculations on direct
sentiment dict or indirect main algorithm.
"""
function evaluate_sentiment_dict_accuracy(chunk=:none)
    if chunk == :none
        first_chunk = first(gdelt_iterator(100_000, 1))
    else
        first_chunk = chunk
    end
    sentiment_dict = sentiment_dict_gdelt(first_chunk)
    agreement = sentiment_dict_to_agreement(sentiment_dict)
    graph_sd, source_map_sd, sign_map_sd = dict_to_graph(agreement)
    census_sentiment_dict = triadic_census(graph_sd, sign_map_sd)
    graph_m, source_map_m, sign_map_m = compute_agreement_graph(first_chunk)
    census_main_algorithm = triadic_census(graph_m, sign_map_m)
    return census_sentiment_dict == census_main_algorithm
end

@test evaluate_sentiment_dict_accuracy()
